import { binder, view, ViewGroup } from "bibl";
import SceneService from "./state.js";
import template from './template.html';
import style from './style.scss';

@view({
    className: "src-scene",
    style,
    template,
    dataset: {
        service: SceneService
    }
})
class Scene extends ViewGroup {
    @binder('select')
    onselect({ item }) {
        this.dispatchEvent('next', item)
    }

    @binder('done')
    ondone() {
        this.dispatchEvent("done");
    }
}

export default Scene;