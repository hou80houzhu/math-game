import { Service, action } from "bibl";

class SceneService extends Service {
	data() {
		return {
			dots: [],
			result: []
		};
	}

	@action('next')
	next(current, { dots, result }) {
		current.dots = dots;
		current.result = result;
	}
}

export default SceneService;