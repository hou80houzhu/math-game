import { Service, action } from "bibl";

class MentalService extends Service {
	data() {
		return {
			size: 10,
			size2: 1,
			_dots: [],
			_result: [],
			_statistic: {}
		};
	}

	@action('next')
	next(current, { dots, result, statistic }) {
		current._dots = dots;
		current._result = result;
		current._statistic = statistic;
	}

	@action('timeUpdate')
	timeUpdate(current, statistic) {
		current._statistic = statistic;
	}
}

export default MentalService;