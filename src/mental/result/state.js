import { Service, action, frames } from "bibl";
import { frame, delay } from 'bibl/std/delay';

class ResultService extends Service {
	data() {
		return {
			list: [],
			statistic: {},
			_in: false,
			_blank: false,
			_display: false
		};
	}

	@action('display')
	display(current) {
		current._display = true;
	}

	@frames('delay-in', 'box-in', 'delay-out')
	oncreate(a, b) {
		super.oncreate(a, b);
	}

	@frames('box-out', 'delay-out')
	onunload() { }

	@action('delay-in')
	delayIn() {
		return frame();
	}

	@action('delay-out')
	delayOut() {
		return delay(300);
	}

	@action('box-in')
	boxIn(current) {
		current._in = true;
	}

	@action('box-out')
	boxOut(current) {
		current._in = false;
	}
}

export default ResultService;