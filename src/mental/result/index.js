import { binder, view, View } from "bibl";
import ResultService from "./state.js";
import template from './template.html';
import style from './style.scss';
import correctIcon from './../../base/icons/right.icon';
import wrongIcon from './../../base/icons/wrong.icon';

@view({
    className: "src-result",
    style,
    template,
    dataset: {
        service: ResultService
    }
})
class Result extends View {
    icons() {
        return [correctIcon, wrongIcon];
    }

    @binder("replay")
    replay(){
        this.dispatchEvent("replay");
    }

    @binder('gotoWelcome')
    gotoWelcome(){
        this.dispatchEvent('gotoWelcome');
    }
}

export default Result;