import { binder, view, View } from "bibl";
import WelcomeService from "./state.js";
import template from './template.html';
import style from './style.scss';

@view({
    className: "src-welcome",
    style,
    template,
    dataset: {
        service: WelcomeService
    }
})
class Welcome extends View {
    @binder("goto")
    goto({item}){
        this.dispatchEvent('goto',item);
    }
}

export default Welcome;