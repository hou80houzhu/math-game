import { Service } from "bibl";

class WelcomeService extends Service {
	data() {
		return {
			list: [
				{ name: "口算50题(10以内)", type: "mentalA" },
				{ name: "口算10题(20以内)", type: "mentalB" },
				{ name: "唐诗三百首", type: "poetry" }
			]
		};
	}
}

export default WelcomeService;