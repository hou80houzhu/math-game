export const sounds = {
    correct: "/math-game/audio/correct.mp3",
    wrong: "/math-game/audio/wrong.mp3"
}