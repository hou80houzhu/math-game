import { view, View } from "bibl";
import PoetryService from "./state.js";
import template from './template.html';
import style from './style.scss';
import { PoetryUtils } from "../utils/PoetryUtils.js";

@view({
    className: "src-poetry",
    style,
    template,
    dataset: {
        service: PoetryService
    }
})
class Poetry extends View {
    onready() {
        PoetryUtils.getContent().then(list => {
            this.commit("setContent", list[20].content);
        });
    }
}

export default Poetry;