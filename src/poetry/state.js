import { Service, action } from "bibl";

class PoetryService extends Service {
	data() {
		return {
			content: []
		};
	}

	@action('setContent')
	setContent(current, content) {
		current.content = content;
	}
}

export default PoetryService;