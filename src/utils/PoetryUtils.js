export const PoetryUtils = {
    content: null,
    contentPs: null,
    getContent() {
        if (this.content == null) {
            if (this.contentPs == null) {
                this.contentPs = fetch('/poetryClean.json').then(a => a.json()).then(list => {
                    this.content = list.map(a => {
                        a.content.forEach(b => {
                            b.forEach(c => {
                                if (!c.dot) {
                                    c.dot = c.word == '·';
                                }
                            })
                        });
                        return a;
                    });
                });
            }
            return this.contentPs.then(() => this.content);
        }
        return Promise.resolve(this.content);
    }
}