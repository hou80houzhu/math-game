class Num {
    constructor(num) {
        this.num = num;
    }

    getLabel() {
        return this.num + '';
    }
}

class Operator {
    constructor(type) {
        this.type = type;
    }

    getLabel() {
        if (this.type == 1) {
            return '+';
        } else {
            return '-';
        }
    }
}

class Exps {
    constructor(list) {
        this.list = list;
    }

    getList(){
        return this.list.map(a=>a.getLabel());
    }

    getValue() {
        let r = this.list.map(a => a.getLabel()).join('');
        return new Function(`return ${r}`).call();
    }

    getResult(size = 5) {
        let rr = NumUtils.getRandomNumsArray([this.getValue()], size - 1);
        return NumUtils.shuffle(rr);
    }
}

export const NumUtils = {
    getRandomNum(size = 1) {
        return Math.round(Math.random() * size * 10)
    },
    getRandomNums(size = 2, size2 = 1) {
        return Array.from(Array(size)).map(() => this.getRandomNum(size2));
    },
    getRandomOperate() {
        return this.getRandomNum() > 5 ? 1 : 0;
    },
    getRandomExp(size = 2, size2 = 1) {
        let r = [];
        this.getRandomNums(size, size2).forEach((a, b) => {
            r.push(new Num(a));
            r.push(new Operator(this.getRandomOperate()));
        });
        r.pop();
        return new Exps(r);
    },
    getExp(size = 2, size2 = 1) {
        while (true) {
            let exp = this.getRandomExp(size, size2);
            if (exp.getValue() > 0) {
                return exp;
            }
        }
    },
    getRandomNumsArray(origin = [], size = 4) {
        let r = [...origin];
        Array.from(new Array(size)).forEach(() => {
            while (true) {
                let a = this.getRandomNum(2);
                if (!r.includes(a)) {
                    r.push(a);
                    break;
                }
            }
        });
        return r;
    },
    shuffle(arr) {
        var len = arr.length;
        for (var i = 0; i < len - 1; i++) {
            var index = parseInt(Math.random() * (len - i));
            var temp = arr[index];
            arr[index] = arr[len - i - 1];
            arr[len - i - 1] = temp;
        }
        return arr;
    }
};