export const TimeUtils = {
    formatOffset(time) {
        let leave1 = time % (24 * 3600 * 1000);
        let hours = Math.floor(leave1 / (3600 * 1000))

        let leave2 = leave1 % (3600 * 1000);
        let minutes = Math.floor(leave2 / (60 * 1000));

        let leave3 = leave2 % (60 * 1000);
        let seconds = Math.round(leave3 / 1000);

        let r = '';
        if (hours) {
            r += `${hours} 小时 `;
        }
        if (minutes) {
            r += `${minutes} 分 `;
        }
        if (seconds) {
            r += `${seconds} 秒`;
        }

        return r;
    }
}