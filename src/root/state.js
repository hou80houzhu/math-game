import { Service } from "bibl";

class RootService extends Service {
	data() {
		return {};
	}
}

export default RootService;