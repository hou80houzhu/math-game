import { view, BondViewGroup, handler } from "bibl";
import RootService from "./state.js";
import template from './template.html';
import style from './style.scss';
import Mental from './../mental';
import Welcome from './../welcome';
import Peotry from './../poetry';

@view({
    className: "src-root",
    style,
    template,
    dataset: {
        service: RootService
    }
})
class Root extends BondViewGroup {
    onready() {
        this.addChild(Welcome);
    }

    @handler('goto')
    goto({ data }) {
        if (data.type == 'mentalA') {
            this.addChild(Mental, {
                parameter: {
                    size: 50,
                    size2: 1
                }
            }).then(a => this.scene = a);
        } else if (data.type == 'mentalB') {
            this.addChild(Mental, {
                parameter: {
                    size: 10,
                    size2: 2
                }
            }).then(a => this.scene = a);
        } else if (data.type == 'poetry') {
            this.addChild(Peotry).then(a => {
                this.scene = a;
            });
        }
    }

    @handler('gotoWelcome')
    gotoWelcome() {
        if (this.scene) {
            this.removeChild(this.scene);
        }
        this.scene = null;
    }
}

export default Root;