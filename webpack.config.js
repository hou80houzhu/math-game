const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    mode: "development",
    entry: "./index.js",
    plugins: [
        new HtmlWebpackPlugin({
            template: 'index.html',
            inject: 'body',
            filename: './index.html'
        }),
        new CopyWebpackPlugin({
            patterns: [
                { from: path.resolve(__dirname, './source'), to: path.resolve(__dirname, './dist') }
            ]
        })
    ],
    devServer: {
        host: '0.0.0.0',
        port: 8080,
        https: true,
    },
    output: {
        path: path.resolve(__dirname, "./dist"),
        filename: "index.js"
    },
    module: {
        rules: [
            {
                test: /.js$/, use: [{
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            "@babel/preset-env"
                        ],
                        plugins: [
                            [
                                "@babel/plugin-proposal-decorators",
                                { "legacy": true }
                            ]
                        ]
                    }
                }]
            },
            { test: /\.html$/i, loader: "html-loader" },
            { test: /\.icon$/i, loader: path.resolve(__dirname, './node_modules/bibl/webpack/loader/icon.js') },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    "to-string-loader",
                    "css-loader",
                    { loader: "sass-loader", options: { implementation: require("sass") } }
                ]
            },
        ]
    }
}