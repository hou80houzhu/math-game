import { Project } from 'bibl';
import Root from './src/root/index';
import style from './src/base/reset.scss';

Project.getContext().then(ctx => {
    Project.executeStyle(style);
    ctx.boot(Root);
});